import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import Courses from './pages/Courses'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'

import './App.css';

function App() {
  return (
  	<Fragment>
    	<AppNavbar/>
	    <Container>
	    	<Login/>
	    	<br></br>
	    	<Register/>
	    	<Home/>
	    	<Courses/>
	    </Container>		
  	</Fragment>
  );
}

export default App;
