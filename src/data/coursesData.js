const coursesData = [
	{
		id: 'wdc001',
		name: "PHP-Laravel",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nam veritatis vero saepe, deleniti voluptas aut asperiores ad quod quisquam eos, non labore quis dicta, ipsam maxime nemo sit vel eveniet?",
		price: 45000,
		onOffer: true
	},
	{
		id: 'wdc002',
		name: "Python-Django",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nam veritatis vero saepe, deleniti voluptas aut asperiores ad quod quisquam eos, non labore quis dicta, ipsam maxime nemo sit vel eveniet?",
		price: 45000,
		onOffer: true
	},
	{
		id: 'wdc003',
		name: "Java - Springboot",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nam veritatis vero saepe, deleniti voluptas aut asperiores ad quod quisquam eos, non labore quis dicta, ipsam maxime nemo sit vel eveniet?",
		price: 60000,
		onOffer: true
	},
]

export default coursesData;